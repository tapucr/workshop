package com.example.springbootworkshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

	@Autowired
	private ProjectRepository projectRepository;

	@GetMapping("/test")
	public Iterable<Project> getAll() {
		return projectRepository.findAll();
	}

}
