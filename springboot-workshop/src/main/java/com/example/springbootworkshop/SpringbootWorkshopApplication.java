package com.example.springbootworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class SpringbootWorkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWorkshopApplication.class, args);
	}

}
