package com.example.springbootworkshop.fileupload;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
public class FileUploadController {

	@Autowired
	private FileUploadRepository fileRepository;

	@Autowired
	private FileUploadService fileUploadService;

	@PostMapping("/upload")
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam("name") String name, @RequestParam("email") String email) {

		try {
			if (!fileUploadService.isFileTypeAllowed(file)) {
				return ResponseEntity.badRequest().body("Only JPEG and PNG files are allowed");
			}

			FileUploadEntity fileUploadEntity = new FileUploadEntity();
			fileUploadEntity.setEmail(email);
			fileUploadEntity.setName(fileUploadService.getFileName(file));
			fileUploadEntity.setData(file.getBytes());
			fileRepository.save(fileUploadEntity);

			return ResponseEntity.ok("File uploaded successfully");
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload file");
		}
	}

	@GetMapping("/{fileId}")
	public ResponseEntity<Resource> get(@PathVariable final Long fileId) {
		Optional<FileUploadEntity> fileOptional = fileRepository.findById(fileId);
		if (fileOptional.isPresent()) {
			FileUploadEntity fileUploadEntity = fileOptional.get();
			ByteArrayResource resource = new ByteArrayResource(fileUploadEntity.getData());

			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + fileUploadEntity.getName() + "\"")
					.contentType(MediaType.IMAGE_JPEG) // Set the appropriate content type
					.body(resource);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/uploadFileWithFromData")
	public ResponseEntity<String> uploadFileWithFromData(@RequestBody UserEvent userEvent) {

		return ResponseEntity.status(HttpStatus.ACCEPTED).body(userEvent.getEmail());
	}

}