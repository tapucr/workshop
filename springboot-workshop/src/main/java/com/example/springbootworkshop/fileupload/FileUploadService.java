package com.example.springbootworkshop.fileupload;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileUploadService {

	private static final List<String> ALLOWED_FILE_TYPES = Arrays.asList("image/jpeg", "image/png");

	public boolean isFileTypeAllowed(final MultipartFile file) {
		String fileType = file.getContentType();
		return fileType != null && ALLOWED_FILE_TYPES.contains(fileType);
	}

	public String getFileName(final MultipartFile file) {
		return StringUtils.cleanPath(file.getOriginalFilename());
	}
}
