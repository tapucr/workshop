package com.example.springbootworkshop.fileupload;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserEvent {

	private String name;
	private String email;
	
}
